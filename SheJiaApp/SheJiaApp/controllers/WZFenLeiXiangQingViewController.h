//
//  WZFenLeiXiangQingViewController.h
//  SheJiaApp
//
//  Created by Mac on 16/6/27.
//  Copyright © 2016年 Mac. All rights reserved.
//

#import "ViewController.h"
#import "WZFenLeiXiangQingViewController.h"
@protocol WZFenLeiXiangQingViewControllerDelegate <NSObject>
@end
@interface WZFenLeiXiangQingViewController : ViewController
@property (nonatomic,strong)NSString *str;
@property (weak, nonatomic) IBOutlet UICollectionView *Collection;
- (IBAction)QuanBuPinPaiBtn:(UIButton *)sender;
- (IBAction)XiangBaoBtn:(UIButton *)sender;
- (IBAction)TuiJianPaiXuBtn:(UIButton *)sender;
@property (weak, nonatomic)id<WZFenLeiXiangQingViewControllerDelegate>delegate;
@end
