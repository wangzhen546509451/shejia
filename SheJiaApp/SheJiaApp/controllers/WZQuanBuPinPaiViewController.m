//
//  WZQuanBuPinPaiViewController.m
//  SheJiaApp
//
//  Created by Mac on 16/6/28.
//  Copyright © 2016年 Mac. All rights reserved.
//
#import <AFNetworking.h>
#import "WZQuanBuPinPaiViewController.h"
#import "WZQuanBuPinPaiTableViewCell.h"
#import <UIKit+AFNetworking.h>
@interface WZQuanBuPinPaiViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSMutableArray *PinPaiarr,*PinPaiImageArr;
@property (nonatomic,strong)NSDictionary *dic;
@end

@implementation WZQuanBuPinPaiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view from its nib.
    self.PinPaiarr = [[NSMutableArray alloc]init];
    self.PinPaiImageArr= [[NSMutableArray alloc]init];
    [_PinPaiarr addObject:@"其他品牌"];
    for (int i=32; i>0; i--) {
        NSString *str=[NSString stringWithFormat:@"brandicon_%d",i];
        [_PinPaiarr addObject:str];
    }

    
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    [manager POST:@"http://www.luxusj.com:8080/brand/search" parameters:@{@"keyword" : @"0"} progress:^(NSProgress * _Nonnull uploadProgress) {
//        NSLog(@"在加载中");
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        self.dic =responseObject;
//        //[self.dic ]
//        NSLog(@"%@",self.dic);
//        self.tab.delegate =self;
//        self.tab.dataSource =self;
//        
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"%@",error);
//    }];
    
    
    
    
    
    
}
- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden=NO;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_PinPaiarr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WZQuanBuPinPaiTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"QuanBuPinPaiCell"];
    if(!cell){
        cell=[[NSBundle mainBundle]loadNibNamed:@"WZQuanBuPinPaiTableViewCell" owner:nil options:nil][0];
    }
    cell.imaView.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",_PinPaiarr[indexPath.row]]];
    cell.MyLab.text=[NSString stringWithFormat:@"%d",indexPath.row];
    //[cell.imaView setImageWithURL:self.dic]
    return cell;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)GoBackBtn:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
