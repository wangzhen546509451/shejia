//
//  WZTuiJianPaiXuViewController.m
//  SheJiaApp
//
//  Created by Mac on 16/6/28.
//  Copyright © 2016年 Mac. All rights reserved.
//

#import "WZTuiJianPaiXuViewController.h"
#import "WZPaiXuFangShiTableViewCell.h"
@interface WZTuiJianPaiXuViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSArray *PaiXuArr;
@end

@implementation WZTuiJianPaiXuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.PaiXuArr = [[NSArray alloc]initWithObjects:@"推荐排序",@"最新发布",@"价格最低",@"价格最高", nil];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_PaiXuArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WZPaiXuFangShiTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PaiXuCell"];
    if(!cell){
        cell=[[NSBundle mainBundle]loadNibNamed:@"WZPaiXuFangShiTableViewCell" owner:nil options:nil][0];
    }
    cell.textLabel.text=_PaiXuArr[indexPath.row];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
