//
//  WZFenLeiXiangQingViewController.m
//  SheJiaApp
//
//  Created by Mac on 16/6/27.
//  Copyright © 2016年 Mac. All rights reserved.
//

#import "WZFenLeiXiangQingViewController.h"
#import "WZFenLeiXiangQingCollectionViewCell.h"
#import "WZQuanBuPinPaiViewController.h"
#import "WZXiangBaoViewController.h"
#import "WZTuiJianPaiXuViewController.h"
@interface WZFenLeiXiangQingViewController ()
@property (nonatomic,strong)NSMutableArray *PingPaiarr;

@end

@implementation WZFenLeiXiangQingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.PingPaiarr=[[NSMutableArray alloc]init];
    for (int i=32; i>=0; i--) {
        NSString *str=[NSString stringWithFormat:@"brandicon_%d",i];
        [_PingPaiarr addObject:str];
    }
    _Collection.backgroundColor = [UIColor whiteColor];
    [_Collection registerNib:[UINib nibWithNibName:@"WZCollectionViewCell" bundle:[NSBundle mainBundle]]forCellWithReuseIdentifier:@"GradientCell"];
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationItem.title=self.str;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WZFenLeiXiangQingCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GradientCell" forIndexPath:indexPath];
    if(!cell){
        cell=[[NSBundle mainBundle]loadNibNamed:@"WZCollectionViewCell" owner:nil options:nil][0];
    }
    
    cell.imaView.image=[UIImage imageNamed:_PingPaiarr[indexPath.row]];
    
    return cell;

}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_PingPaiarr count];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)QuanBuPinPaiBtn:(UIButton *)sender {
    WZQuanBuPinPaiViewController *view=[[WZQuanBuPinPaiViewController alloc]init];
    [self.navigationController pushViewController:view animated:YES];
    
}

- (IBAction)XiangBaoBtn:(UIButton *)sender {
    WZXiangBaoViewController *view=[[WZXiangBaoViewController alloc]init];
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)TuiJianPaiXuBtn:(UIButton *)sender {
    WZTuiJianPaiXuViewController *view=[[WZTuiJianPaiXuViewController alloc]init];
    [self.navigationController pushViewController:view animated:YES];
    
}
@end
